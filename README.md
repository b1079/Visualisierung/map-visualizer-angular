# MapVisualizer

## Inhalt
Enhält die Möglichkeit zum Visualisieren von Routen der verschiedenen Anbietern auf einer Karte, sowie die Lokalisation bei Adress Eingabe.


## Ausführen mit 
``ng serve``
## Voraussetzung 
Vorausgesetzt ist ein laufender Endpoint(https://gitlab.com/b1079/rest-interfaces/generic-endpoint) und eine gesetzte und valide URL in ``enviroments/enviroment.ts`` / ``enviroments/enviroment.prod.ts``.

### Bilder

![Visualisation Foto](img/img.png)

![Route](img/img_3.png)


![Visualisation Foto](img/img_2.png)


