import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelektionComponent } from './selektion.component';

describe('SelektionComponent', () => {
  let component: SelektionComponent;
  let fixture: ComponentFixture<SelektionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelektionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelektionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
