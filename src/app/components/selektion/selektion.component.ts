import {Component, OnInit} from '@angular/core';
import {SelectedLocationsService} from '../../services/selected-locations.service';
import {ModelLocationInput} from '../../model/ModelLocationInput';
import {Router} from '@angular/router';
import {MapConnectorService} from '../../services/map-connector.service';
import {GenericConnectorService} from '../../services/generic-connector.service';
import {Route} from '../../model/Route';
import {InformationServiceService} from '../../services/information-service.service';

@Component({
  selector: 'app-selektion',
  templateUrl: './selektion.component.html',
  styleUrls: ['./selektion.component.css']
})
/**
 * menu for the adding addresses
 */
export class SelektionComponent implements OnInit {


  constructor(public selectionService: SelectedLocationsService,
              private router: Router,
              private mapConnector: MapConnectorService,
              private genericSender: GenericConnectorService,
              private infoservice: InformationServiceService) {
  }

  ngOnInit(): void {

  }


  add(): void {
    // place for standard Information
    const index = this.selectionService.add(new ModelLocationInput({}));
    this.selectionService.select(index - 1);
    this.router.navigate([{outlets: {'left-menu': ['data']}}]);
  }

  onSelect(index: number): void {
    this.selectionService.select(index);
    this.router.navigate([{outlets: {'left-menu': ['data']}}]);
  }

  public async calcRoute(): Promise<void> {
    const map = this.mapConnector.getMap();
    for (let i = 0; i < this.selectionService.locations.length; i++) {
      try {
        const res = await this.genericSender.getAddressLocation(this.selectionService.locations[i - 1], this.selectionService.locations[i]);
        this.infoservice.getInformationService().displayInformation(res);
        for (let j = 0; j < res.length; j++) {
          const possibleRoute = res[j];
          if (possibleRoute != null && possibleRoute.geoJsonLineString != null) {
            await map.drawGenericRoute(possibleRoute, j);
          }
        }
      } catch (e) {
      }


    }
  }

}
