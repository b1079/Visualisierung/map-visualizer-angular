import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-floating-menu',
  templateUrl: './floating-menu.component.html',
  styleUrls: ['./floating-menu.component.css']
})
/**
 * Floating menu information -- show left or right
 */
export class FloatingMenuComponent implements OnInit {

  lastLocation = '';
  @Input()
  public align = 'right';

  constructor(private router: Router) {

  }

  ngOnInit(): void {
  }

  back(): void {
    this.router.navigate(['..']);
  }

}
