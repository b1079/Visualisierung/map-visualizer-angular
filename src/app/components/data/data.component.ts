import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {LocationInput} from '../../model/LocationInput';
import {SelectedLocationsService} from '../../services/selected-locations.service';
import {ModelLocationInput} from '../../model/ModelLocationInput';
import {GenericConnectorService} from '../../services/generic-connector.service';
import {MapConnectorService} from '../../services/map-connector.service';
import {LatLng} from 'leaflet';
import {InformationServiceService} from '../../services/information-service.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  public formGroup = new FormGroup({
    address: new FormControl(''),
    city: new FormControl(''),
    postalCode: new FormControl(''),
    houseNumber: new FormControl('')
  });
  @Output()
  emitter = new EventEmitter<LocationInput>();


  constructor(private selectedLocationService: SelectedLocationsService,
              private sendService: GenericConnectorService,
              private mapConnector: MapConnectorService,
              private informationService: InformationServiceService) {
  }

  ngOnInit(): void {
    console.log('Init');
    this.formGroup.setValue(this.selectedLocationService.selectedLocation);
  }

  /**
   * Loads the information and displays the located positions on the map
   * @param event
   */
  async onSubmit(event): Promise<void> {
    console.log(this.formGroup.value);
    const res = await this.sendService.getLocationPosition(this.formGroup.getRawValue());
    console.dir(res);
    const map = this.mapConnector.getMap();
    let i = 0;
    for (const entry of res) {

      if (entry != null) {
        console.dir(entry);
        const position = new LatLng(entry.location.latitude, entry.location.longitude);
        console.dir(position);
        map.addPoint(position,
          (this.selectedLocationService.selectedIndex) * 100 + i, entry.typeOrigin);

        map.moveTo(position);
      }
      i += 1;
    }
    this.informationService.getInformationService().clear();
    this.informationService.getInformationService().displayInformationLocation(res);

    this.selectedLocationService.setLocation(new ModelLocationInput(this.formGroup.getRawValue()));

  }

}

