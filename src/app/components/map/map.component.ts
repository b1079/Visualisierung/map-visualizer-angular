import {Component, OnInit, ViewChild} from '@angular/core';
import * as L from 'leaflet';
import {circle, GeoJSON, LatLng, latLng, marker, polygon, tileLayer} from 'leaflet';
import {BackendConnectorService} from '../../services/backend-connector.service';
import {Route} from '../../model/Route';
import {MapConnectorService} from '../../services/map-connector.service';
import {GenericRoute} from '../../model/GenericRoute';
import {GenericConnectorService} from '../../services/generic-connector.service';
import {GoldenAngleColors} from '../../model/util/Colors';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
/**
 * Map component renders map. And contains functions for the interaction with the map.
 */
export class MapComponent implements OnInit {

  constructor(private backendService: BackendConnectorService,
              private mapConnector: MapConnectorService,
              private genericConnector: GenericConnectorService) {
    this.mapConnector.connect(this);
  }

  layers = [];
  options = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 18})
    ],
    zoom: 12,
    center: latLng(49.8042, 8.1939)
  };

  map: L.Map;
  markers = {};
  myStyle = {
    color: '#ff7800',
    weight: 5,
    opacity: 0.65
  };
  private myColors = new GoldenAngleColors(10, 65, 100);

  drawnRoutes = {};

  ngOnInit(): void {
    this.mapConnector.connect(this);
  }

  onMapReady(map: L.Map): void {
    this.map = map;
  }

  public moveTo(coords: LatLng, zoom = 18): void {
    if (this.map !== undefined) {
      this.map.flyTo(coords, zoom);
    }
  }

  public addRoute(route: Route, key: string = ''): void {
    if (this.drawnRoutes[key] != null) {
      // tslint:disable-next-line:no-shadowed-variable
      const savedGeoJson: GeoJSON = this.drawnRoutes[key];
      savedGeoJson.removeFrom(this.map);
    }

    const geojson = L.geoJSON(route.geometry as any, {style: this.myStyle});

    geojson.addTo(this.map);
    this.drawnRoutes[key] = geojson;

    console.dir(route.distance);
    console.dir(route.duration);
  }

  public addPoint(coords: LatLng, i: number, name: string): void {
    console.log(coords);
    this.removePoint(i);
    this.markers[i] = L.marker(coords);
    this.markers[i].bindPopup('Marker: ' + name + ' ' + (Math.floor(i / 100)));
    this.markers[i].addTo(this.map);
  }

  public removePoint(i: number): void {
    if (this.markers[i]) {
      this.map.removeLayer(this.markers[i]);
    }

  }

  public async calcRoute(): Promise<void> {
    const marker1: L.Marker = this.markers[1];
    const marker2: L.Marker = this.markers[2];
    const res = await this.backendService.getRoute(marker1.getLatLng(), marker2.getLatLng());

    if (res != null) {
      this.addRoute(res);
    }

  }

  public async drawGenericRoute(genericRoute: GenericRoute, id: number): Promise<void> {
    const key = genericRoute.originType + ': ' + id;
    // if (genericRoute.originType === 'GoogleAPI') {
    const coords = genericRoute.geoJsonLineString.coordinates;
    // for (let i = 0; i < coords.length; i++) {
    //   const coordPoint = coords[i];
    //   this.addPoint(latLng(coordPoint[1], coordPoint[0]), i, 'LineString');
    // }
    if (this.drawnRoutes[key] != null) {
      // tslint:disable-next-line:no-shadowed-variable
      const savedGeoJson: GeoJSON = this.drawnRoutes[key];
      savedGeoJson.removeFrom(this.map);
    }
    const styleInfo = {
      color: this.myColors.colors[id],
      weight: 5,
      opacity: 0.65
    };

    const geojson = L.geoJSON(genericRoute.geoJsonLineString as any, {style: styleInfo});
    geojson.bindPopup('Route: ' + genericRoute.originType);
    geojson.addTo(this.map);
    this.drawnRoutes[key] = geojson;

    // }

  }


}
