import {Component, OnInit, ViewChild} from '@angular/core';
import {MapComponent} from '../map/map.component';
import {DataComponent} from '../data/data.component';

import {LocationInput} from '../../model/LocationInput';
import {BackendConnectorService} from '../../services/backend-connector.service';
import {latLng} from 'leaflet';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
/**
 * Main view shows map and floating menus
 */
export class MainViewComponent implements OnInit {

  @ViewChild(MapComponent)
  map: MapComponent;

  @ViewChild(DataComponent)
  dataComponent: DataComponent;


  constructor(private connector: BackendConnectorService) {
  }

  ngOnInit(): void {
  }


  calcRoute(): void{
    this.map.calcRoute();
  }

}
