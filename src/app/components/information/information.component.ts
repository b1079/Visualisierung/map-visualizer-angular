import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {GenericRoute} from '../../model/GenericRoute';
import {GeocodingResponse} from '../../model/GeocodingResponse';
import {InformationServiceService} from '../../services/information-service.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
/**
 * Display for additional information of the calculated routes / geolocations
 */
export class InformationComponent implements OnInit {

  public routes: GenericRoute[] = [];
  public points: GeocodingResponse[] = [];
  public mode: string;

  constructor(private infoservice: InformationServiceService) {
    infoservice.connect(this);
  }

  ngOnInit(): void {
    this.infoservice.connect(this);
  }

  public displayInformation(information: GenericRoute[]): void {
    this.clear();
    this.mode = 'routes';
    this.routes.push(...information);

  }

  public displayInformationLocation(information: GeocodingResponse[]): void {
    this.clear();
    this.mode = 'locations';
    this.points.push(...information);


  }

  clear(): void {
    this.mode = 'reset';
    this.points.splice(0, this.points.length);
    this.routes.splice(0, this.points.length);

  }

}
