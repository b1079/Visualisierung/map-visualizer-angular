import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LocationInput} from '../../model/LocationInput';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
/**
 * Menu for the input of addresses and calculate routes.
 */
export class MenuComponent implements OnInit {

  @Output()
  location1Emitter = new EventEmitter<LocationInput>();

  @Output()
  location2Emitter = new EventEmitter<LocationInput>();
  showFirst = true;
  showSecond = false;
  @Output()
  calculationEmitter = new EventEmitter<boolean>();

  constructor() {
  }

  expandAddress(i: number): void {
    console.log(i);
    if (i === 1) {
      this.showFirst = true;
      this.showSecond = false;
    } else {
      this.showFirst = false;
      this.showSecond = true;
    }
  }

  public emmitRoute(): void {
    this.calculationEmitter.emit(true);
  }

  public emmitLocation(location: LocationInput, emitter: EventEmitter<LocationInput>): void {
    emitter.emit(location);
  }


  ngOnInit(): void {
  }

}
