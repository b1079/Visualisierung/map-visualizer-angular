import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MenuComponent} from './components/menu/menu.component';
import {DataComponent} from './components/data/data.component';
import {SelektionComponent} from './components/selektion/selektion.component';
import {InformationComponent} from './components/information/information.component';


const routes: Routes = [
  {
    path: '', component: SelektionComponent, outlet: 'left-menu'
  },
  {
    path: '', component: InformationComponent, outlet: 'right-menu'
  },
  {
    path: 'data', component: DataComponent, outlet: 'left-menu'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
