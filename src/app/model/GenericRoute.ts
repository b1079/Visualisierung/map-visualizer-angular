import {RouteInformation} from './util/RouteInformation';
import {GeoJSONLineString} from './GeoJSONLineString';

/**
 * Wrapper for GenericRoute from API + orginType for assoziation with the delivering service
 */
export class GenericRoute {
  public metadata: object;
  public routeInformation: RouteInformation;
  public geoJsonLineString: GeoJSONLineString;
  public originType: string;

}
