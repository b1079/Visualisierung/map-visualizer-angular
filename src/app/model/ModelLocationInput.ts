import {LocationInput} from './LocationInput';

export class ModelLocationInput implements LocationInput {
  address = '';
  city = '';
  houseNumber = '';
  postalCode = '';

  constructor(partial: Partial<ModelLocationInput>) {
    Object.assign(this, partial);
  }


}
