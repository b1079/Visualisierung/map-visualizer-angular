/**
 * GeoJSONLineString which should describe the route
 */
export class GeoJSONLineString {
  type: string;
  properties: {};
  coordinates: Array<Array<number>>;
}
