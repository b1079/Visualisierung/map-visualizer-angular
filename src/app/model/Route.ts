import {GeoJSONLineString} from './GeoJSONLineString';

/**
 * route information for the defined service
 */
export class Route {
  distance: number;
  duration: number;
  geometry: GeoJSONLineString;
}
