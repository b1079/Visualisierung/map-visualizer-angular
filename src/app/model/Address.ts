import {LocationInput} from './LocationInput';

/**
 * Address from the api
 */
export class Address {
  street: string;
  city: string;
  postalCode: string;
  houseNumber: string;

  static fromLocationInput(input: LocationInput): Address {
    const address = new Address();
    address.city = input.city;
    address.street = input.address;
    address.postalCode = input.postalCode;
    address.houseNumber = input.houseNumber;
    return address;
  }

}
