/**
 * @deprecated
 */
export interface NominatimResponse {
  osm_type: string;
  lat: string;
  lon: string;
  display_name: string;
  importance: number;
  address: string;
}
