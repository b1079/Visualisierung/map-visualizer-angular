import {Address} from './Address';

export class GeocodingResponse {
  location: { latitude: number, longitude: number };
  address: Address;
  typeOrigin: string;

}
