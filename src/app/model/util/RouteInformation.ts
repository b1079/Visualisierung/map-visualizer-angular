export class RouteInformation{
  public distance: number;
  public travelTime: number;
}
