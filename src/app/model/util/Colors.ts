export class GoldenAngleColors {
  private goldenAngle = 80 * (3 - Math.sqrt(5));
  private colorsArray: Array<string> = [];

  constructor(private readonly size: number,
              private readonly brightness: number,
              private readonly saturation: number = 100) {
    this.size = Math.floor(size);
    this.generateColors();

  }

  get colors(): Array<string> {
    return this.colorsArray;
  }

  private generateColors(): void {
    for (let i = 0; i < this.size; i++) {
      this.colorsArray.push(this.hslToHex(i * this.goldenAngle + 60, this.saturation, this.brightness));
    }
  }

  private hslToHex(h, s, l): string {
    l /= 100;
    const a = s * Math.min(l, 1 - l) / 100;
    const f = n => {
      const k = (n + h / 30) % 12;
      const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
      return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
    };
    return `#${f(0)}${f(8)}${f(4)}`;
  }

}
