/**
 * Request information for the api
 */
export interface LocationInput {
  address: string;
  city: string;
  postalCode: string;
  houseNumber: string;
}
