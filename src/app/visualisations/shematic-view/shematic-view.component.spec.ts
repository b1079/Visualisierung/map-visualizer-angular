import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShematicViewComponent } from './shematic-view.component';

describe('ShematicViewComponent', () => {
  let component: ShematicViewComponent;
  let fixture: ComponentFixture<ShematicViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShematicViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShematicViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
