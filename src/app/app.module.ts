import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {MapComponent} from './components/map/map.component';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';

import {DataComponent} from './components/data/data.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MainViewComponent} from './components/main-view/main-view.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MenuComponent} from './components/menu/menu.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {FloatingMenuComponent} from './components/floating-menu/floating-menu.component';
import {SelektionComponent} from './components/selektion/selektion.component';
import {CardTemplateComponent} from './components/template/card-template/card-template.component';
import {CommonModule} from '@angular/common';
import { InformationComponent } from './components/information/information.component';
import { ShematicViewComponent } from './visualisations/shematic-view/shematic-view.component';


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    DataComponent,
    MainViewComponent,
    MenuComponent,
    FloatingMenuComponent,
    SelektionComponent,
    CardTemplateComponent,
    InformationComponent,
    ShematicViewComponent,

  ],
  imports: [
    BrowserModule,
    LeafletModule,
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
}
