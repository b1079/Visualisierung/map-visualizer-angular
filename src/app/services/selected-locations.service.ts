import {Injectable} from '@angular/core';
import {LocationInput} from '../model/LocationInput';
import {ModelLocationInput} from '../model/ModelLocationInput';

@Injectable({
  providedIn: 'root'
})
/**
 * helper to manage selected locations
 */
export class SelectedLocationsService {

  locations: Array<LocationInput> = [];
  selectedLocation: LocationInput;
  selectedIndex: number;

  constructor() {
    this.locations.push(new ModelLocationInput({}));
  }

  public add(input: LocationInput): number {
    this.locations.push(input);

    return this.locations.length;
  }

  public select(index: number): void {
    this.selectedLocation = this.locations[index];
    this.selectedIndex = index;
  }

  public remove(index: number): void {
    this.locations.splice(index, 1);
  }

  get locationInputs(): Array<LocationInput> {
    return this.locations;
  }

  public setLocation(locationInput: LocationInput): void {
    Object.assign(this.selectedLocation, locationInput);
    this.locations[this.selectedIndex] = locationInput;
  }


  get length(): number {
    return this.locations.length;
  }



}
