import { TestBed } from '@angular/core/testing';

import { GenericConnectorService } from './generic-connector.service';

describe('GenericConnectorService', () => {
  let service: GenericConnectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenericConnectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
