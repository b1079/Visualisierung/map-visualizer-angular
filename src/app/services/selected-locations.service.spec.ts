import { TestBed } from '@angular/core/testing';

import { SelectedLocationsService } from './selected-locations.service';

describe('SelectedLocationsService', () => {
  let service: SelectedLocationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SelectedLocationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
