import {Injectable} from '@angular/core';
import {InformationComponent} from '../components/information/information.component';

@Injectable({
  providedIn: 'root'
})
/**
 * helper to connect services
 */
export class InformationServiceService {
  private component: InformationComponent;

  constructor() {
  }

  public connect(component: InformationComponent): void {
    this.component = component;
  }

  public getInformationService(): InformationComponent {
    return this.component;
  }

}
