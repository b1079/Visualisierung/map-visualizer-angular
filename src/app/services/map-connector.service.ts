import {Injectable} from '@angular/core';
import {MapComponent} from '../components/map/map.component';

@Injectable({
  providedIn: 'root'
})
/**
 * helper to connect to map
 */
export class MapConnectorService {
  map: MapComponent;

  constructor() {
  }

  public connect(map: MapComponent): void {
    this.map = map;
  }

  public getMap(): MapComponent {
    return this.map;
  }
}
