import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {LocationInput} from '../model/LocationInput';
import {NominatimResponse} from '../model/NominatimResponse';
import {environment} from '../../environments/environment';
import {GenericRoute} from '../model/GenericRoute';
import {GeocodingResponse} from '../model/GeocodingResponse';

@Injectable({
  providedIn: 'root'
})
/**
 * connection service to the generic api
 */
export class GenericConnectorService {
  private services: Array<string> = [];
  private geocoders: Array<string> = [];

  constructor(private http: HttpClient) {
    this.init();
  }

  public getServiceName(id: number): string {
    return this.services[id];
  }

  public getGeocoderName(id: number): string {
    return this.services[id];
  }

  private async init(): Promise<void> {
    this.services = await this.getServices();
    this.geocoders = await this.getGeocoders();
  }

  public async getAddressLocation(from: LocationInput, to: LocationInput): Promise<GenericRoute[]> {
    const fromObj = this.getLocationObject(from);
    const toObj = this.getLocationObject(to);
    const requests: Array<RouteWrapper> = [];

    const obj = {
      from: fromObj,
      to: toObj,
      api: ''
    };

    for (const item of this.services) {
      obj.api = item;
      const request: Promise<GenericRoute> = this.http.post<GenericRoute>(
        environment.url + '/generic/route',
        obj).toPromise();
      requests.push({promise: request, type: item});
    }
    const arr = [];

    for (const entry of requests) {
      try {
        const res = await entry.promise;
        res.originType = entry.type;
        arr.push(res);
      } catch (e) {
        arr.push(null);
      }
    }

    return arr;
  }

  public async getLocationPosition(from: LocationInput): Promise<GeocodingResponse[]> {
    const requests: Array<LocationWrapper> = [];
    for (const geocoder of this.geocoders) {
      const req = this.http.post<GeocodingResponse>(environment.url + '/generic/geocoding',
        {api: geocoder, location: this.getLocationObject(from)}).toPromise();
      requests.push({promise: req, type: geocoder});
    }
    const arr = [];

    for (const entry of requests) {
      try {
        const res = await entry.promise;
        res.typeOrigin = entry.type;
        arr.push(res);
      } catch (e) {
        arr.push(null);
      }
    }

    return arr;
  }


  private getLocationObject(location: LocationInput): object {
    const param = {
      houseNumber: location.houseNumber,
      street: location.address,
      city: location.city,
      country: 'Deutschland'
    };
    return param;
  }

  private async getServices(): Promise<Array<string>> {

    return this.http.get<Array<string>>(environment.url + '/generic/services/routing').toPromise();
  }

  private async getGeocoders(): Promise<Array<string>> {

    return this.http.get<Array<string>>(environment.url + '/generic/services/geocoding').toPromise();
  }

}

interface RouteWrapper {
  promise: Promise<GenericRoute>;
  type: string;
}

interface LocationWrapper {
  promise: Promise<GeocodingResponse>;
  type: string;
}
