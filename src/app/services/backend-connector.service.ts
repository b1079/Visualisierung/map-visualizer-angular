import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {NominatimResponse} from '../model/NominatimResponse';
import {LocationInput} from '../model/LocationInput';
import {Route} from '../model/Route';

@Injectable({
  providedIn: 'root'
})
/**
 * @deprecated
 */
export class BackendConnectorService {

  constructor(private http: HttpClient) {
  }

  public getAddressLocation(location: LocationInput): Promise<NominatimResponse[]> {

    const param = {
      postalcode: location.postalCode,
      housenumber: location.houseNumber,
      address: location.address,
      city: location.city,
      country: 'Deutschland'
    };

    return this.http.get<NominatimResponse[]>(environment.url + '/api/v1/nominatim', {params: param}).toPromise();

  }

  public getRoute(res1: L.LatLng, res2: L.LatLng): Promise<Route> {

    const param = {
      lat: res1.lat + '',
      lon: res1.lng + '',
      latTo: res2.lat + '',
      lonTo: res2.lng + ''
    };

    return this.http.get<Route>(environment.url + '/api/v1/osrm', {params: param}).toPromise();

  }


}


