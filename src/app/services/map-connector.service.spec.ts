import { TestBed } from '@angular/core/testing';

import { MapConnectorService } from './map-connector.service';

describe('MapConnectorService', () => {
  let service: MapConnectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapConnectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
